const SerialPort = require('serialport');
var Readline = SerialPort.parsers.Readline;
var readline = require('readline');
const inquirer = require('inquirer');
var util = require('util');

var rl = readline.createInterface(process.stdin, process.stdout);

let menuCommandAT = [
    'Setear APN de Claro',
    'Setear APN de Hologram',
    'Preguntar APN',
    'Intensidad de Sennal',
    'Operador de Red',
    'GPRS Network',
    'Check Pin',
    'Tipo de Tecnologia',
    'Bandas de Frecuencia',
    'Ativa GPRS coneccion',
    'Chequea IP Dinamica',
    'Chequa DNS Primario',
    'Modo Avion',
    'Modo Normal',
    'Activar PDP',
    'Tipo de Tecnologia Extendida',
    'Lista de Codigos de Operadores',
    'Caracteres Permitidos',
    'Bandas de trabajo'
];

var listofports = [];
SerialPort.list().then(function (ports) {
    let serialPortChoices = {};
    serialPortChoices.type = 'list';
    serialPortChoices.message = 'Choose serial port you would like to connect:'
    serialPortChoices.name = "serialport";

    let listOfdevices = [];
    ports.forEach(function (port) {
        listOfdevices.push(port.path);
    })


    inquirer.prompt([{
        type: 'list',
        name: 'port',
        message: 'Selecciona el puerto',
        choices: listOfdevices,
    }, {
        type: 'list',
        name: 'baudrate',
        message: 'Seleccione la razon de baudios',
        choices: ['115200', '9600'],
    }])

        .then(answers => {
            const answersBaudRate = parseInt(answers.baudrate, 1)
            openPortAndOutputData(answers.port, answersBaudRate);
        })

});

function openPortAndOutputData(serialport, answersBaudRate) {
    var port = new SerialPort(serialport, {
        baudRate: answersBaudRate
    }, function (err) {
        if (err) {
            return console.log('Error: ', err.message)
        }
    });
    console.log('Port ' + serialport + ' On!');
    console.log(' ');



    console.log("   Digite un numero");
    // port.write('AT+CSQ\r');

    inquirer.prompt({
        type: 'rawlist',
        name: 'command',
        message: 'Selecciones una consulta',
        choices: menuCommandAT,
    })
        .then(answers => {
            console.log(JSON.stringify(answers.command));

            if (JSON.stringify(answers.command) == 'Intensidad de Sennal') {
                port.write('AT+CSQ\r');
                console.log("holaaaaaaaaa");
            }

        })

    var parser = new Readline()
    port.pipe(parser)
    parser.on('data', function (data) {
        console.log('SARA-U201: ' + data)
        // data handling 
    })
};


function printMenu() {
    menuCommandAT.forEach(function (elemento, indice, array) {
        if ((indice + 1) <= 9)
            process.stdout.write("0");
        console.log((indice + 1) + " " + elemento);
    })
}

